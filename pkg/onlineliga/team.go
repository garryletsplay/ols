package onlineliga

import (
	"log"
	"sort"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/utils"
)

type Team struct {
	ID      int      `json:"team_id"`
	Name    string   `json:"team_name"`
	Players []Player `json:"players"`
}

func (team *Team) GetName(teamInfoDoc *goquery.Document) {
	selection := teamInfoDoc.Find(".team-overview-teamname-big > .hidden-md > .ol-team-name")
	name := strings.TrimSpace(selection.Text())

	//debug logs
	// log.Println(name)

	team.Name = name
}

func playerIDs(teamSquadDoc *goquery.Document) []int {
	var playerIDs []int

	teamSquadDoc.Find(".ol-player-name").Each(func(i int, s *goquery.Selection) {
		playerID := parsePlayerID(s.AttrOr("onclick", "nope"))
		playerIDs = append(playerIDs, playerID)
	})

	//debug logs
	// log.Printf("player IDs: %v", playerIDs)

	return playerIDs
}

func TeamPlayers(team *Team, teamSquadDoc *goquery.Document) {
	players := playerIDs(teamSquadDoc)

	for _, playerID := range players {
		player := Player{
			ID: playerID,
		}
		team.Players = append(team.Players, player)
	}

	//debug logs
	// log.Println(team.Players)
}

func (team *Team) GetPlayers(teamSquadDoc *goquery.Document) {
	players := playerIDs(teamSquadDoc)

	for _, playerID := range players {
		player := Player{
			ID: playerID,
		}
		team.Players = append(team.Players, player)
	}

	//debug logs
	// log.Println(team.Players)
}

func parsePlayerID(target string) int {
	splitted := strings.SplitAfter(target, "playerId: ")
	trimmed := strings.TrimRight(splitted[len(splitted)-1], " });")
	id, err := strconv.ParseInt(trimmed, 0, 64)
	if err != nil {
		log.Println(err, "no player ID found in: ", target)
		return 0
	}
	return int(id)
}

// team functions
func (team Team) MarketValue() (float64, float64) {
	var values []float64
	for _, player := range team.Players {
		values = append(values, player.MarketValue)
	}
	return utils.SumAvgFloat64(values)
}

func (team Team) Salary() (float64, float64) {
	var values []float64
	for _, player := range team.Players {
		values = append(values, player.Salary)
	}
	return utils.SumAvgFloat64(values)
}

func (team Team) Age() (float64, float64) {
	var values []float64
	for _, player := range team.Players {
		values = append(values, player.Age())
	}
	return utils.SumAvgFloat64(values)
}

func (team Team) ExpiringContracts() int {
	var expiringContracts int
	for _, player := range team.Players {
		if player.ContractExpiring {
			expiringContracts += 1
		}
	}
	return expiringContracts
}

func (team Team) PlayersByPosition(positions []string) []Player {
	var players []Player

	for _, player := range team.Players {
		for _, position := range positions {
			if stringInSlice(position, player.Positions) {
				players = append(players, player)
			}
		}
	}

	sort.Slice(players, func(i, j int) bool {
		return players[i].Overall > players[j].Overall
	})
	return players
}

func (team Team) Top15Players() []Player {
	var players []Player

	for _, player := range team.Players {
		if !stringInSlice("Torwart", player.Positions) {
			players = append(players, player)
		}
	}

	sort.Slice(players, func(i, j int) bool {
		return players[i].Overall > players[j].Overall
	})

	if len(players) >= 15 {
		return players[:15]
	}
	return players
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
