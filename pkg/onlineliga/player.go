package onlineliga

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/convert"
	"gitlab.com/onlineliga/ols/pkg/status"
)

type Player struct {
	ID               int                `json:"id"`
	TeamID           int                `json:"team_id"`
	Name             string             `json:"name"`
	Country          string             `json:"country"`
	BirthSeason      float64            `json:"birth_season"` // 24
	BirthWeek        float64            `json:"birth_week"`   // 38
	Salary           float64            `json:"salary"`       // -
	MarketValue      float64            `json:"market_value"` // -
	Weight           float64            `json:"weight"`       // 35
	Height           float64            `json:"height"`       // 27
	Talent           float64            `json:"talent"`
	Fitness          float64            `json:"fitness"`
	Overall          float64            `json:"overall"`
	TalentDiscovered bool               `json:"talent_discovered"`
	ContractExpiring bool               `json:"contract_expiring"`
	Positions        []string           `json:"positions"`
	Abilities        map[string]float64 `json:"abilities"`
}

func playerMap(doc *goquery.Document, findQuery string) map[string]string {
	selection := doc.Find(findQuery)
	values := make(map[string]string)
	selection.Each(func(i int, s *goquery.Selection) {
		key := strings.TrimSpace(s.Children().First().Text())
		value := strings.TrimSpace(s.Children().Last().Text())
		values[key] = value
	})

	//debug logs
	// log.Println(values)

	return values
}

// profile mapping
const (
	Name        = iota
	Country     = iota
	TeamName    = iota
	Age         = iota
	Position    = iota
	Weight      = iota
	Height      = iota
	BirthWeek   = iota
	MarketValue = iota
	Foot        = iota
	Salary      = iota
	Contract    = iota
	Loyalty     = iota
	Bookings    = iota
)

func playerArray(doc *goquery.Document, findQuery string) []string {
	selection := doc.Find(findQuery)
	var values []string
	selection.Each(func(i int, s *goquery.Selection) {
		value := strings.TrimSpace(s.Children().Last().Text())
		values = append(values, value)
	})

	//debug logs
	// log.Println(values)

	return values
}

func (player *Player) GetTeamID(profileDoc *goquery.Document) {
	var teamID int
	selection := profileDoc.Find(".ol-team-name")
	anchor := selection.First().AttrOr("onclick", "nope")
	teamID = parseUserId(anchor)

	//debug logs
	// log.Printf("team ID: %v", teamID)
	player.TeamID = teamID
}

func (player *Player) GetProfile(profileDoc *goquery.Document) {
	currentSeason := status.CurrentInformation.Season
	currentWeek := status.CurrentInformation.Week

	abilities := playerMap(profileDoc, ".ol-playerabilitys-table-row")
	profile := playerArray(profileDoc, ".ol-player-table-row")
	age := convert.ProfileValueToFloat64(profile[Age], true)

	player.Name = profile[Name]
	player.Country = profile[Country]
	player.BirthWeek = convert.ProfileValueToFloat64(profile[BirthWeek], false) - 1 // a player in OL grows older one week before the shown birth week
	player.BirthSeason = calculateBirthSeason(age, player.BirthWeek, currentWeek, currentSeason)
	player.Height = convert.ProfileValueToFloat64(profile[Height], true)
	player.Weight = convert.ProfileValueToFloat64(profile[Weight], true)
	player.ContractExpiring = strings.Contains(profile[Contract], fmt.Sprintf("Saison %v", currentSeason)) ||
		strings.Contains(profile[Contract], fmt.Sprintf("season %v", currentSeason))
	player.Positions = playerPositions(profile[Position])
	player.MarketValue = convert.CurrencyToFloat64(profile[MarketValue])
	player.Salary = convert.CurrencyToFloat64(profile[Salary])

	player.Fitness = convert.AbilityToFloat64(abilities["Fitness"])
	player.Talent = convert.TalentToFloat64(abilities["Talent"])
	player.TalentDiscovered = !strings.Contains(abilities["Talent"], "ermittelt") && !strings.Contains(abilities["Talent"], "determined")

	overall, ok := abilities["FÄHIGKEITEN"]
	if !ok {
		overall = abilities["SKILLS"]
	}
	player.Overall = convert.AbilityToFloat64(overall)

	player.Abilities = convert.AbilityMapToAbilities(abilities)

	//debug logs
	// log.Println(utils.JSONPrint(player))
}

func playerPositions(profileValue string) []string {
	var positions []string
	splitted := strings.Split(profileValue, ", ")
	for _, element := range splitted {
		position := element
		positions = append(positions, position)
	}
	return positions
}

func (player Player) Age() float64 {
	currentWeek := status.CurrentInformation.Week
	currentSeason := status.CurrentInformation.Season

	var roofedAge float64
	if player.BirthSeason < 0 {
		roofedAge = player.BirthSeason*-1 + currentSeason
	} else {
		roofedAge = player.BirthSeason + currentSeason
	}

	partly := (44-player.BirthWeek+currentWeek)/44 - 1

	return roofedAge + partly
}

func calculateBirthSeason(age float64, birthWeek float64, currentWeek float64, currentSeason float64) float64 {
	if currentWeek >= birthWeek {
		return currentSeason - age
	} else {
		return currentSeason - (age + 1)
	}
}
