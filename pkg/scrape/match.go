package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/request"
	"gitlab.com/onlineliga/ols/internal/response"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
)

func FetchMatchDocument(match *onlineliga.Match, navigation string) *goquery.Document {
	matchOptions := &request.RequestOption{
		Path: fmt.Sprintf("/match%s", navigation),
		QueryParameters: map[string]string{
			"season":  fmt.Sprintf("%v", match.Season),
			"matchId": fmt.Sprintf("%v", match.ID),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(matchOptions))
	doc := response.ResponseToDocument(res)
	return doc
}
