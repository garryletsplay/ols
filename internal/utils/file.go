package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"strings"
	"time"
)

func ToJSONFile(input interface{}, identifier string, path string) {
	// if path is empty (default) create a generic file name
	path = determineFileName(identifier, path)

	// convert input structs into prettified json
	json_data := JSONPrettyBytes(input)

	// error handling
	err := ioutil.WriteFile(path, json_data, 0644)
	if err != nil {
		log.Fatalln("failed writing data to JSON file.", err)
	}
}

func determineFileName(identifier string, path string) string {
	// replace whitespaces with underscores for unified file name
	identifier = strings.Replace(identifier, " ", "_", -1)

	if len(path) == 0 {
		path = "./" +
			time.Now().Format("2006_01_02_15-04") +
			"_" + identifier +
			".json"
	}
	return path
}

func JSONPrettyBytes(input interface{}) []byte {
	jsonString, err := json.MarshalIndent(input, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	return jsonString
}
