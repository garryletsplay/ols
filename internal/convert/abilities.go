package convert

import (
	"log"
	"strconv"
	"strings"
)

var abilityMap = map[string][]string{
	"right_foot":          {"Rechter Fuß", "Right Foot"},
	"left_foot":           {"Linker Fuß", "Left Foot"},
	"shooting":            {"Schusstechnik", "Shooting"},
	"power":               {"Schusskraft", "Shooting Power"},
	"technique":           {"Technik", "Technique"},
	"pace":                {"Schnelligkeit", "Speed"},
	"heading":             {"Kopfball", "Header"},
	"tackling":            {"Zweikampf", "Duel"},
	"constitution":        {"Athletik", "Athleticism"},
	"condition":           {"Kondition", "Endurance"},
	"game_insight":        {"Taktikverst.", "Tactics"},
	"keeper_on_the_line":  {"Linie", "Reflexes"},
	"keeper_off_the_line": {"Rauslaufen", "One on One"},
	"keeper_box":          {"Strafraum", "Penalty area"},
	"keeper_foot":         {"Fuss", "Kicking"},
	"keeper_game_opening": {"Spieleröffnung", "Build-up"},
	"keeper_sweeper":      {"Libero", "Sweeper"},
}

func AbilityToFloat64(value string) float64 {
	value = strings.Replace(value, "%", "", -1)
	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		//debug log
		// log.Println(err, "failed to parse ability. original string: ", value)
		return 0
	}
	return valueAsFloat64
}

func TalentToFloat64(value string) float64 {
	if strings.Contains(value, "nicht") || strings.Contains(value, "Not") {
		return 0
	}

	splits := strings.Split(value, "%")
	value = splits[0]

	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Println(err, "failed to parse talent. original string: ", value)
		return 0
	}
	return valueAsFloat64
}

func AbilityMapToAbilities(input map[string]string) map[string]float64 {
	formattedMap := make(map[string]float64)
	for key, value := range abilityMap {
		val, ok := input[value[0]]
		if !ok {
			val, ok = input[value[1]]
			if !ok {
				val = "0%"
				// log.Println("Neither of these keys could be found", value)
			}
		}
		formattedMap[key] = AbilityToFloat64(val)
	}

	// debug log
	// utils.JSONPrint(formattedMap)

	return formattedMap
}
