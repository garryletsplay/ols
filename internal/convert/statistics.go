package convert

import (
	"log"
	"strconv"
	"strings"
)

func StatisticValueToInt(value string, delimiter string) int {
	value = strings.Replace(value, delimiter, "", -1)
	valueAsInt, err := strconv.ParseInt(value, 0, 32)
	if err != nil {
		if len(value) > 0 {
			log.Println(err, "failed to parse statistics field:", value, delimiter)
		}
		return 0
	}
	return int(valueAsInt)
}

func BookingToColour(value string) string {
	if strings.Contains(value, "yellow") {
		return "yellow"
	} else {
		return "red"
	}
}
