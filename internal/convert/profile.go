package convert

import (
	"log"
	"strconv"
	"strings"
)

func ProfileValueToFloat64(value string, leftSide bool) float64 {
	splitted := strings.SplitAfter(value, " ")
	if len(splitted) == 0 {
		log.Println("failed to parse a profile field because it is empty")
		return 0
	}
	var returnIndex int = 0
	if leftSide {
		returnIndex = 0
	} else {
		returnIndex = len(splitted) - 1
	}

	value = strings.TrimSpace(splitted[returnIndex])
	value = strings.Replace(value, ",", ".", -1)

	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		if len(value) > 0 {
			log.Println(err, "failed to parse profile field:", value)
		}
		return 0
	}
	return valueAsFloat64
}

var replacementStrings = []string{
	" €",   // de
	" £",   // co.uk
	" CHF", // ch
	"€ ",   // at
	".",
	",",
}

func CurrencyToFloat64(value string) float64 {
	for _, toBeReplaced := range replacementStrings {
		value = strings.Replace(value, toBeReplaced, "", -1)
	}

	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Println(err)
		return 0
	}
	return valueAsFloat64
}
