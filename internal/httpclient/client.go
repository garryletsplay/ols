package httpclient

import (
	"net/http"
)

var DefaultClient = &http.Client{
	Jar: Cookie(),
	Transport: &http.Transport{
		MaxIdleConnsPerHost: 20,
	},
	Timeout: 0,
	// Timeout: 10 * time.Second,
}
