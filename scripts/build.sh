#!/bin/bash

if [[ "${OLS_VERSION}" == "" ]]; then
    OLS_VERSION="dev"
fi

if [[ ! -z "${CI_COMMIT_TAG}" ]]; then
    OLS_VERSION="${CI_COMMIT_TAG}"
fi

BUILD_FLAGS="-X 'gitlab.com/onlineliga/ols/cmd.Version=${OLS_VERSION}'"

for PLATFORM in "linux/amd64" "darwin/arm64" "darwin/amd64" "windows/amd64"; do
    GOOS=${PLATFORM%/*}
    GOARCH=${PLATFORM#*/}

    BIN_FILE_NAME="binaries/ols-${GOOS}-${GOARCH}"

    if [[ "${GOOS}" == "windows" ]]; then
        BIN_FILE_NAME="${BIN_FILE_NAME}.exe"
    fi

    echo Building ${GOOS}-${GOARCH} version ${OLS_VERSION}...

    GOOS="${GOOS}" GOARCH="${GOARCH}" BUILD_FLAGS="${BUILD_FLAGS}" \
    go build -o "${BIN_FILE_NAME}" -ldflags "${BUILD_FLAGS}" \
    || FAILURES="${FAILURES} ${GOOS}-${GOARCH}"
done

echo Build Failures:

for FAILURE in $FAILURES; do
    echo $FAILURE
done
