<!-- ABOUT THE PROJECT -->
## About The Project

___OLS___ (_OL scraper_) is a webscraper written in [golang][golang] which is meant to fetch data
from the football manager browser game __[Onlineliga][onlineligade]__.

Currently capable of scraping:
- league
- team
- player
- match (WIP)

Currently supported targets:
- [Onlineliga DE][onlineligade]
- [Onlineliga AT][onlineligaat]
- [Onlineliga CH][onlineligach]
- [Onlineleague UK][onlineleagueuk]

![intro][ols_league]
<!-- TABLE OF CONTENTS -->
## Table of Contents

- [About The Project](#about-the-project)
- [Table of Contents](#table-of-contents)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Disclaimer](#disclaimer)
  - [Quickstart](#quickstart)
  - [Prerequisites](#prerequisites)
- [Installation](#installation)
  - [Install with golang (recommended)](#install-with-golang-recommended)
  - [Install binaries](#install-binaries)
    - [Windows Powershell](#windows-powershell)
    - [Bash](#bash)
- [Downloads](#downloads)
- [Usage](#usage)
  - [Windows](#windows)
  - [Linux](#linux)
- [Examples](#examples)
- [Contributing](#contributing)
- [Contact](#contact)

### Built With

- [golang][golang]
- [vscode][vscode]

<!-- GETTING STARTED -->
## Getting Started

The scraper fetches data and saves the output by default into a JSON file at the exact same directory
where the application is executed.

Additionally it can analyze OL teams according to [Boonlight's Excel sheets][boonlight-excel]
and dump the result into a CSV file.

### Disclaimer

| :exclamation: Disclaimer                                               |
| :--------------------------------------------------------------------- |
| Scraping and analyzing this game is not allowed by _Terms of Service_. |
![ToS][tos_image]
- [_AGB DACH_][agb]
- [_ToS UK_][tos]

### Quickstart

Install the OL scraper and open the help menu
```bash
go install gitlab.com/onlineliga/ols@latest
ols --help
```

### Prerequisites
Optional:
- [golang][golang]

<!-- Installation -->
## Installation

There are two options to run the application.
Either by using [golang][golang] or the compiled binaries which you can find under [releases][releases].

### Install with golang (recommended)
You can install the application with [golang][golang] specifying a [release][releases].
```bash
go install gitlab.com/onlineliga/ols@v?.?.?
```
To install the latest version use `@latest`. Works on every platform.

### Install binaries
Following examples will install `latest` version

#### Windows Powershell

installation with [Powershell][powershell]:
- create directory and remove curl alias
  ```powershell
  mkdir $HOME\ols\
  Remove-Item alias:curl
  ```
- fetch latest tag and download binaries
  ```powershell
  $JSON = curl -s https://gitlab.com/api/v4/projects/31154129/repository/tags | ConvertFrom-Json
  $TAG = $JSON[0].name
  curl -o $HOME\ols\ols.exe -L https://gitlab.com/onlineliga/ols/-/jobs/artifacts/$TAG/raw/binaries/ols-windows-amd64.exe?job=build-release
  ```

#### Bash
installation with bash:
- create directory
  ```bash
  mkdir -p ~/.local/bin/
  ```
- fetch latest tag and download binaries
  ```bash
  TAG=$(curl -s  https://gitlab.com/api/v4/projects/31154129/repository/tags | jq -r .[0].name)
  curl -o ~/.local/bin/ols -L https://gitlab.com/onlineliga/ols/-/jobs/artifacts/${TAG}/raw/binaries/ols-linux-amd64?job=build-release
  ```
- make binaries executable
  ```bash
  chmod +x ~/.local/bin/ols
  ```

___Note___: in the [Downloads](#downloads) section you can find binaries for all available platforms

<!-- Downloads -->
## Downloads
___OLS___ is crosscompiled for
- MacOS
  - [64-bit][macos64-bin]
  - [64-bit ARM][macosarm64-bin]
- Linux
  - [64-bit][linux64-bin]
- Windows
  - [64-bit][win64-bin]

<!-- Usage -->
## Usage

### Windows
Add the application directory to your `PATH` environment variable so you can call it from anywhere.
- [Powershell][powershell]:
  ```powershell
  $env:Path += ";$HOME\ols\"
  ```
___Note:___ In order to make it persistent visit [Microsoft docs][docs-path].

Now you can use the application like this:
```powershell
ols --help
```
### Linux
```bash
ols --help
```

<!-- Examples -->
## Examples
[Powershell examples][ps_examples]

<!-- CONTRIBUTING -->
## Contributing


<!-- CONTACT -->
## Contact

Community Discord server
- https://discord.gg/B5jB3ESByF


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[golang]: https://golang.org/
[golanginstall]: https://golang.org/doc/install#install
[vscode]: https://code.visualstudio.com/
[onlineligade]: https://www.onlineliga.de/
[onlineligaat]: https://www.onlineliga.at/
[onlineligach]: https://www.onlineliga.ch/
[onlineleagueuk]: https://www.onlineleague.co.uk/
[releases]: https://gitlab.com/onlineliga/ols/-/releases
[powershell]: https://docs.microsoft.com/en-us/powershell/
[cmd]: https://www.netzwelt.de/tutorial/163585-windows-10-so-startet-eingabeaufforderung-cmd-0511.html
[releases]: https://gitlab.com/onlineliga/ols/-/releases
[docs-path]: https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/
[agb]: https://www.onlineliga.de/#url=gtc
[tos]: https://www.onlineleague.co.uk/#url=gtc

[win64-bin]: https://gitlab.com/onlineliga/ols/-/jobs/artifacts/main/raw/binaries/ols-windows-amd64.exe?job=build-release
[linux64-bin]: https://gitlab.com/onlineliga/ols/-/jobs/artifacts/main/raw/binaries/ols-linux-amd64?job=build-release
[macosarm64-bin]: https://gitlab.com/onlineliga/ols/-/jobs/artifacts/main/raw/binaries/ols-darwin-arm64?job=build-release
[macos64-bin]: https://gitlab.com/onlineliga/ols/-/jobs/artifacts/main/raw/binaries/ols-darwin-amd64?job=build-release

[boonlight-excel]: https://docs.google.com/spreadsheets/d/1OGGqdE6Dy9YLImkxwzxboNQ-ylNoKFLKoYwKaGgPMM0/edit?usp=sharing

<!-- IMAGES -->
[ols_league]: docs/img/ols_league.gif
[tos_image]: docs/img/ToS.png

<!-- PATH LINKS -->
[ps_examples]: docs/examples/README.md
